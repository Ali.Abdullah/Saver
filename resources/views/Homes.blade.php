<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Savers App</title>
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css')}}">
    <link rel="stylesheet" href="{{ asset('fonts/font-awesome.min.css')}}">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">    <link rel="stylesheet" href="{{ asset('fonts/ionicons.min.css')}}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Bitter:400,700">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="{{ asset('css/Contact-Form.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Footer.css') }}">
    <link rel="stylesheet" href="{{ asset('css/Header.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/css/lightbox.min.css">
    <link rel="stylesheet" href="{{ asset('css/Lightbox-Gallery.css') }}">
    <link href="{{ asset('css/Navigation.css') }}" rel="stylesheet">
    <link href="{{ asset('css/Footer.css') }}" rel="stylesheet">
    <link href="{!! asset('css/styles.css') !!}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/Testimonials.css') }}">

</head>



<body>
<div>
    <nav class="navbar navbar-light navbar-expand-md navigation">
        <div class="container"><a href="#" class="navbar-brand">Saver</a><button data-toggle="collapse" data-target="#navcol-1" class="navbar-toggler"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse"
                id="navcol-1">
                <ul class="nav navbar-nav mr-auto">
                    <li role="presentation" class="nav-item"><a href="#home" class="nav-link active">Home</a></li>
                    <li role="presentation" class="nav-item"><a href="#service" class="nav-link">Services</a></li>
                    <li role="presentation" class="nav-item"><a href="#about" class="nav-link">About</a></li>
                    <li role="presentation" class="nav-item"><a href="#shots" class="nav-link">Shots</a></li>
                    <li role="presentation" class="nav-item"><a href="#contact" class="nav-link">Contacts</a>
                    </li>

                </ul><span class="navbar-text actions"><a href="{{ url('/login') }}" class="login">Log In</a><a class="btn btn-light action-button" role="button" href="{{ url('/register') }}">Sign Up</a></span></div>
        </div>
    </nav>
</div>






@include('sections.header');
@include('sections.services');
@include('sections.about');
@include('sections.shots');
@include('sections.testimonials');
@include('sections.contact');










    <div class="footer" style="margin:0;margin-bottom:-244px;">
        <footer>
            <div class="container">
                <div class="row">
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Credentials&nbsp;</h3>
                        <ul>
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Signup</a></li>
                            <li><a href="#contact">Contacts</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-6 col-md-3 item">
                        <h3>Maker</h3>
                        <ul>
                            <li><a href="#about">Idea</a></li>
                            <li><a href="#shots">Savings</a></li>
                            <li><a href="{{ url('/filter') }}">We give you&nbsp;</a></li>
                        </ul>
                    </div>
                    <div class="col-md-6 item text">
                        <h3>Saver</h3>
                        <p>The company is a new in its field , its the idea of savings the money to the people who want to save more money to thier life .</p>
                    </div>
                    <div class="col item social"><a href="#"><i class="icon ion-social-facebook"></i></a><a href="#"><i class="icon ion-social-twitter"></i></a></div>
                </div>
                <p class="copyright">Saver © 2018</p>
            </div>
        </footer>
    </div>

    <script src="{!! asset('js/jquery.min.js')!!}"></script>
    <script src="{!! asset('js/bootstrap.min.js')!!}"></script>
    <script src="{!! asset('js/app.js') !!}"></script>
    <script src="{!! asset('js/script.js') !!}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.8.2/js/lightbox.min.js"></script>
</body>

</html>
