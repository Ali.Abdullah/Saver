@extends('layouts.app')

@section('content')

<div class="container">

  <div class="article">
      <div class="container">
          <div class="row">
              <div class="col-lg-10 col-xl-8 offset-lg-1 offset-xl-2">
                  <div class="intro" class="form-group">
                      <h1 class="text-center" for="usr">{{$articles->title}}</h1>
                      <img src="{!! url($art->image) !!} class="img-fluid post-pic post-img" />
                  </div>
                  <div class="text form-group">
                    {{$articles->body}}
                  </div>
              </div>
          </div>
          <hr>
          <span>Comments</span>&nbsp;&nbsp;
          <span class="danger">7</span>
          &nbsp;&nbsp;
          &nbsp;
          <span>Likes </span>
          &nbsp;&nbsp;
          <span class="danger">5</span>
          <br>
          <hr class="comments-hr">
          <hr class="comments-hr">
          <p class="comments">
             @foreach($articles->comments as $c)
                            <tr>
                                <td>
                                   {{$c->comment}}
                                </td>
                            </tr>
              @endforeach
          </p>
          <hr class="comments-hr">
          <form action="/read/{{$articles->id}}" method="POST">
                  {{csrf_field()}}
                  <div class="form-group">
          <textarea rows="6" cols="50" name="body" form="usrform" style="margin-top:20px;" placeholder="Write Comment here"></textarea>
                  </div>
          <button class="btn add-art btn-primary" type="button" style="margin-bottom:26px; ">Add Comemnt</button>
          </form>

@endsection
