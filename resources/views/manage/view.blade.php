@extends('layouts.app')

@section('content')

  <div class="article-list">
  <div class="container" style="margin-top:33px;">
          <div class="intro">
              <h2 class="text-center">Latest Articles</h2>
              <p class="text-center">The latest article that talk about how you can save money</p>
          </div>
            <tr>
                <td> Title</td>
            </tr>

            @foreach($art as $art)
                <div class="col-sm-6 col-md-4 item"><a href="#"><img class="img-fluid" src="{!! (string)($art->image) !!}"></a>
                    <a clas="name" href="{{ "/read/".$art->id}}">{{$art->title}}</a>
                    <p class="description">{{$art->body}}</p><a href="#" class="action"><i class="fa fa-arrow-circle-right"></i></a>
                  </div>
            @endforeach
        <button class="btn add btn-primary" href ="add" type="button" style="margin-bottom:26px; ">Add Article</button>
    </div>
</div>
</div>

@endsection
