
  @extends('layouts.app')

  @section('content')

 <div class="container">
              <div class="row">
                  <div class="col-md-12"><h2 class="add-article">Add article</h2>
      <br><br>
      <form action="add" method="post">
          {{ csrf_field() }}
      <div class="form-group">
      <p>Title :</p>
      <input type="text" class="form-control" name ="title" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter title">
      <br><br>
      <p>Article Image :</p>
      <input type="file" name="image">
      <br><br>
      <p>Topic :</p>
      <textarea rows="6" cols="50" name="body" id="exampleInputEmail1" aria-describedby="emailHelp" form="usrform" placeholder="Write article here"></textarea>
      <input class="btn add-art btn-primary" type="submit" value="Add Article" style="margin-bottom:26px; ">
      </div>
    </form>
              </div>
          </div>`
  </div>

  @endsection
