<div class="testimonials" style="margin-bottom:57px;">
    <div class="container">
        <div class="intro">
            <h2 class="text-center">Testimonials </h2>
            <p class="text-center">What Our clients says about saver and its services</p>
        </div>
        <div class="row people">
            <div class="col-md-6 col-lg-4 item">
                <div class="box">
                    <p class="description">&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<br>publishing
                        software like Aldus Pag<br></p>
                </div>
                <div class="author"><img class="rounded-circle" src="{{asset('images/اشةيغ.jpg')}}">
                    <h5 class="name">Mohamed Hamdy</h5>
                    <p class="title">5r Div Company</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 item">
                <div class="box">
                    <p class="description">when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries,<br>but also the leap into electronic ty<br></p>
                </div>
                <div class="author"><img class="rounded-circle" src="{{asset('images/مخاب.jpg')}}">
                    <h5 class="name">Mohab el sayed</h5>
                    <p class="title">Techspace company</p>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 item">
                <div class="box">
                    <p class="description">It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop<br>publishing software like Aldus Pag<br></p>
                </div>
                <div class="author"><img class="rounded-circle" src="{{asset('images/شةق.jpg')}}">
                    <h5 class="name">Amr Galal</h5>
                    <p class="title">Sociality company</p>
                </div>
            </div>
        </div>
    </div>
</div>