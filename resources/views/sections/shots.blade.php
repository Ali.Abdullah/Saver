<a id="shots" class="fl-scroll-link"></a>
<div class="photo-gallery">
    <div class="container">
        <div class="intro">
            <h2 class="text-center" style="margin-top:-15px;">Shots</h2>
            <p class="text-center" style="margin-top:-17px;">Choose from the below shots the Picture that fit your way of saving money</p>
        </div>
        <div class="row photos">
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a8.png" data-lightbox="photos"><img src="{{asset('images/a8.png')}}" class="images-fluid  img-thumbnail" style=" min-width: 232px;
                max-height: 235px;"/></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a1.jpg" data-lightbox="photos"><img src="{{asset('images/a1.jpg')}}" class="images-fluid  img-thumbnail"  style="max-width: 305px;
                max-height: 235px;"/></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a2.jpg" data-lightbox="photos"><img src="{{asset('images/a2.jpg')}}" class="images-fluid  img-thumbnail" style="max-width: 305px;
                max-height: 235px;"/></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a3.jpg" data-lightbox="photos"><img src="{{asset('images/a3.jpg')}}" class="images-fluid  img-thumbnail" style="max-width: 305px;
                max-height: 235px;"/></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a4.jpg" data-lightbox="photos"><img src="{{asset('images/a4.jpg')}}" class="images-fluid  img-thumbnail" style="max-width: 305px;
                max-height: 235px;"/></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a5.jpg" data-lightbox="photos"><img src="{{asset('images/a5.jpg')}}" class="images-fluid  img-thumbnail" style="max-width: 305px;
                max-height: 235px;"/></a></div>
            <div class="col-sm-6 col-md-4 col-lg-3 item"><a href="images/a6.jpg" data-lightbox="photos"><img src="{{asset('images/a6.jpg')}}" class="images-fluid  img-thumbnail"  style="max-width: 305px;
                max-height: 235px;"/></a></div>
        </div>
    </div>
</div>