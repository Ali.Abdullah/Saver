<a id="contact" class="fl-scroll-link"></a>
    <div class="contact">
    <form method="post">
        <h2 class="text-center">Contact us</h2>
        <div class="form-group"><input type="text" name="name" placeholder="Name" class="form-control" /></div>
        <div class="form-group"><input type="email" name="email" placeholder="Email" class="form-control" /></div>
        <div class="form-group"><textarea rows="14" name="message" placeholder="Message" class="form-control"></textarea></div>
        <div class="form-group"><button class="btn btn-primary" type="submit">send</button></div>
    </form>
</div>
