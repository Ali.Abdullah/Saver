
  @extends('layouts.app')

  @section('content')


    <div>
    <div class="container filter-page">
        <div class="row">
            <div class="col-md-3">
            <div class="filter">
                <h3>Filters</h3>
                <br>
                <span>Features :</span>
                <br />
              
                <form method="get" action="filter">
                      <select name="features">
                        <option value="much money">much money</option>
                        <option value="enough for living">enough for living</option>
                        <option value="Training">Training</option>
                        <option value="Debt">Debt</option>
                      </select>
                      <br><br>
                      <span>Levels :</span>
                      <br>
                      <select name="levels">
                        <option value="1">lvl 1</option>
                        <option value="2">lvl 2</option>
                        <option value="3">lvl 3</option>
                        <option value="4">lvl 4</option>
                      </select>
                      <br><br>
                      <p class="rating">Maximum Rating :</p>
                      <select name="rating">
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      {{-- <input type="checkbox" name="star" name="rating" value="5-star">
                      <span>5-star</span><br><input type="checkbox" name="rating4" value="4">
                      <span>4-star</span><br><input type="checkbox" name="rating3" value="3">
                      <span>3-star</span><br><input type="checkbox" name="rating2" value="2">
                      <span>2-star</span><br><input type="checkbox" name="rating1" value="1">
                      <span>1-star</span> --}}
                      <br><br>
                      <span>Date :</span>
                      <br><br>
                      Exact date : <br><br><input id="date" name="date" type="date"><br><br>
                      <br>
                      <input class="btn btn-primary" type="submit" value="Add Filter" style="margin-bottom:26px; ">
                </form>
            </div>
            </div>


            <div class="col-md-9"><div class="photo-gallery">
        <div class="row photos">
          @if(is_null($fil) == false)
            @foreach ($fil as $fils)
            <div class="col-sm-4 ite6msat" style="margin-top:10px;"><a href="{!! url($fils->image) !!}" data-lightbox="photos"><img src="{!! url($fils->image) !!}" class="img-fluid img-thumbnail" /></a></div>
            @endforeach
          @endif
        </div>          
    
</div></div>
        </div>
    </div>
</div>

@endsection
