<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Homes');
});
/**Route::get('/comments', function () {
    return view('Comments');
});
Route::get('/filters', function () {
    return view('filter');
});
Route::get('/articles', function () {
    return view('Articlelist');
});
Route::get('/addarticle', function () {
    return view('Addarticle');
});**/

Auth::routes();

Route::get('/logged', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'manage@home');
Route::get('/add','manage@Add');
Route::post('/add','manage@Add');
Route::get('/view','manage@view');
Route::get('/read/{id}','manage@read');
Route::post('/read/{id}','manage@read');
Route::get('/filter','FilterController@filter');
//Route::post('/filter','FilterController@filter');
//Route::get('/insert','FilterController@search');
