<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Comment;
use Auth;
use Storage;
use \Input as Input ;
class manage extends Controller
{
   public function Add (Request $request) {
     if($request->isMethod('post')) {
         $article=new Article();
         $article->title=$request->input('title');
         $article->body=$request->input('body',false);
         $article->user_id=Auth::user()->id;
         $article->save();
         return redirect('view');
       }


        $this->validate($request, [
    	    	'image' => 'mimes:jpeg,bmp,png', //only allow this type extension file.
    		]);

        if(Input::hasfile('file')) {
          $file=Input::file('image');
          $file->move('uploads', $file->getClientOriginalName());
          echo '<image src = "uploads/' . $file->getClientOriginalName() .'"/>';
        }

     return view('manage.Add');
   }
   public function view (){
     $articles=Article::all();
     $arr=Array('art'=>$articles);
     return view('manage.view',$arr);
   }
   public function read(Request $request,$id){
     if ($request->isMethod('post'))
     {
       $comments = new comment();
       $comments->comment=$request->input('body');
       $comments->article_id= $id;
       $comments->save();
     }
     $articles=Article::find($id);
     $arr=Array('articles'=>$articles);
     return view ('manage.read',$arr);
   }
   public function home(){
     return view('Homes');
   }

}
