<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Filter;


class FilterController extends Controller
{
  /*  public function search(){
      for($i=13;$i<=20;$i++)
      {
          $filter=new Filter();
          $pathToFile = "images/sa ".$i.".png";
          $filter->image = $pathToFile;
          $filter->rating  = "this is rating";
          $filter->save();
      }
    return view('insert');
  }  */

    public function filter(Request $request){
      $fil = null;

      $Conditions = array(['features','=', $request->input('features')]);
      if($request->has('features') && $request->input('features') != ""){
        //$fil=Filter::where('features','=', $request->input('features'))->get();
        
      }
      if($request->has('levels') && $request->input('levels') != ""){
        //$fil=$fil::where('levels','=', $request->input('levels'))->get();
        array_push($Conditions,['levels','=', $request->input('levels')]);
      }
      if($request->has('rating') && $request->input('rating') != ""){
        //$fil=$fil::where('rating','=',$request->input('rating'))->get();
        array_push($Conditions,['rating','=',$request->input('rating')]);
      }
      if($request->has('date') && $request->input('date') != ""){
        //$fil=$fil::where('date','=', $request->input('date'))->get();
        array_push($Conditions,['date','=', $request->input('date')]);
      }
      //echo implode("|", $Conditions); 
      $fil=Filter::where($Conditions)->get();
      return view('filter',['fil' => $fil]);

    }
}
